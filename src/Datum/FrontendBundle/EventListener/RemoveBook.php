<?php
namespace Datum\FrontendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Datum\FrontendBundle\Entity\Book;
use Datum\FrontendBundle\Services\MailManager;

class RemoveBook
{
    /**
     * @var mailer
     */
    private $oMailer;

    /**
     * @param MailManager $mailer
     */
    public function __construct(MailManager $oMailer)
    {
        $this->oMailer = $oMailer;
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Book) {          
            $this->oMailer->sendMailToAuthor($entity->getAuthors()->getEmail(), 'Book has been deleted', $entity);            
        }
    }
}

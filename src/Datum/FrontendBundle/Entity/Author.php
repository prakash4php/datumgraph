<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Author
 */
class Author
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $books;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Author
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Author
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add books
     *
     * @param \Datum\FrontendBundle\Entity\Book $books
     * @return Author
     */
    public function addBook(\Datum\FrontendBundle\Entity\Book $books)
    {
        $this->books[] = $books;

        return $this;
    }

    /**
     * Remove books
     *
     * @param \Datum\FrontendBundle\Entity\Book $books
     */
    public function removeBook(\Datum\FrontendBundle\Entity\Book $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBooks()
    {
        return $this->books;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        // Add your code here
    }
    /**
     * @var string
     */
    private $email;


    /**
     * Set email
     *
     * @param string $email
     * @return Author
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $site_payment_gateways;


    /**
     * Add site_payment_gateways
     *
     * @param \Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways
     * @return Author
     */
    public function addSitePaymentGateway(\Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways)
    {
        $this->site_payment_gateways[] = $sitePaymentGateways;

        return $this;
    }

    /**
     * Remove site_payment_gateways
     *
     * @param \Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways
     */
    public function removeSitePaymentGateway(\Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways)
    {
        $this->site_payment_gateways->removeElement($sitePaymentGateways);
    }

    /**
     * Get site_payment_gateways
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSitePaymentGateways()
    {
        return $this->site_payment_gateways;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userextensions;


    /**
     * Add userextensions
     *
     * @param \Datum\FrontendBundle\Entity\UserExtension $userextensions
     * @return Author
     */
    public function addUserextension(\Datum\FrontendBundle\Entity\UserExtension $userextensions)
    {
        $this->userextensions[] = $userextensions;

        return $this;
    }

    /**
     * Remove userextensions
     *
     * @param \Datum\FrontendBundle\Entity\UserExtension $userextensions
     */
    public function removeUserextension(\Datum\FrontendBundle\Entity\UserExtension $userextensions)
    {
        $this->userextensions->removeElement($userextensions);
    }

    /**
     * Get userextensions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserextensions()
    {
        return $this->userextensions;
    }
}

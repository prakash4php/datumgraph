<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book
 */
class Book
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $isbn_code;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Datum\FrontendBundle\Entity\Author
     */
    private $authors;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isbn_code
     *
     * @param string $isbnCode
     * @return Book
     */
    public function setIsbnCode($isbnCode)
    {
        $this->isbn_code = $isbnCode;

        return $this;
    }

    /**
     * Get isbn_code
     *
     * @return string 
     */
    public function getIsbnCode()
    {
        return $this->isbn_code;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Book
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Book
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set authors
     *
     * @param \Datum\FrontendBundle\Entity\Author $authors
     * @return Book
     */
    public function setAuthors(\Datum\FrontendBundle\Entity\Author $authors = null)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return \Datum\FrontendBundle\Entity\Author 
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Add categories
     *
     * @param \Datum\FrontendBundle\Entity\Category $categories
     * @return Book
     */
    public function addCategory(\Datum\FrontendBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Datum\FrontendBundle\Entity\Category $categories
     */
    public function removeCategory(\Datum\FrontendBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if (!$this->getCreatedAt()) {
            $this->created_at = new \DateTime();
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setIsbnCodeVAlue()
    {
        $this->isbn_code = rand().strtotime(date('Y-m-d H:i:s'));
    }
}

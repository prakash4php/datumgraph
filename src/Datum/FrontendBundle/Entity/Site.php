<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 */
class Site
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Site
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $site_payment_gateways;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->site_payment_gateways = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add site_payment_gateways
     *
     * @param \Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways
     * @return Site
     */
    public function addSitePaymentGateway(\Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways)
    {
        $this->site_payment_gateways[] = $sitePaymentGateways;

        return $this;
    }

    /**
     * Remove site_payment_gateways
     *
     * @param \Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways
     */
    public function removeSitePaymentGateway(\Datum\FrontendBundle\Entity\SitePaymentGateways $sitePaymentGateways)
    {
        $this->site_payment_gateways->removeElement($sitePaymentGateways);
    }

    /**
     * Get site_payment_gateways
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSitePaymentGateways()
    {
        return $this->site_payment_gateways;
    }
}

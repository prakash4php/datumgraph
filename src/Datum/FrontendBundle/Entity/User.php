<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userextensions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userextensions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userextensions
     *
     * @param \Datum\FrontendBundle\Entity\UserExtension $userextensions
     * @return User
     */
    public function addUserextension(\Datum\FrontendBundle\Entity\UserExtension $userextensions)
    {
        // echo "here";ezit
        $this->userextensions[] = $userextensions;
        foreach($this->userextensions as $userextension)
        {
            $userextension->setUsers($this);
        }

        return $this;
    }

    /**
     * Remove userextensions
     *
     * @param \Datum\FrontendBundle\Entity\UserExtension $userextensions
     */
    public function removeUserextension(\Datum\FrontendBundle\Entity\UserExtension $userextensions)
    {
        $this->userextensions->removeElement($userextensions);
    }

    /**
     * Get userextensions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserextensions()
    {
        return $this->userextensions;
    }
}

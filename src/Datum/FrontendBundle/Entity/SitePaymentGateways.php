<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SitePaymentGateways
 */
class SitePaymentGateways
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $first_setting;

    /**
     * @var boolean
     */
    private $second_setting;

    /**
     * @var boolean
     */
    private $is_enabled;

    /**
     * @var boolean
     */
    private $is_testmode;

    /**
     * @var \Datum\FrontendBundle\Entity\Site
     */
    private $sites;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_setting
     *
     * @param boolean $firstSetting
     * @return SitePaymentGateways
     */
    public function setFirstSetting($firstSetting)
    {
        $this->first_setting = $firstSetting;

        return $this;
    }

    /**
     * Get first_setting
     *
     * @return boolean 
     */
    public function getFirstSetting()
    {
        return $this->first_setting;
    }

    /**
     * Set second_setting
     *
     * @param boolean $secondSetting
     * @return SitePaymentGateways
     */
    public function setSecondSetting($secondSetting)
    {
        $this->second_setting = $secondSetting;

        return $this;
    }

    /**
     * Get second_setting
     *
     * @return boolean 
     */
    public function getSecondSetting()
    {
        return $this->second_setting;
    }

    /**
     * Set is_enabled
     *
     * @param boolean $isEnabled
     * @return SitePaymentGateways
     */
    public function setIsEnabled($isEnabled)
    {
        $this->is_enabled = $isEnabled;

        return $this;
    }

    /**
     * Get is_enabled
     *
     * @return boolean 
     */
    public function getIsEnabled()
    {
        return $this->is_enabled;
    }

    /**
     * Set is_testmode
     *
     * @param boolean $isTestmode
     * @return SitePaymentGateways
     */
    public function setIsTestmode($isTestmode)
    {
        $this->is_testmode = $isTestmode;

        return $this;
    }

    /**
     * Get is_testmode
     *
     * @return boolean 
     */
    public function getIsTestmode()
    {
        return $this->is_testmode;
    }

    /**
     * Set sites
     *
     * @param \Datum\FrontendBundle\Entity\Site $sites
     * @return SitePaymentGateways
     */
    public function setSites(\Datum\FrontendBundle\Entity\Site $sites = null)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return \Datum\FrontendBundle\Entity\Site 
     */
    public function getSites()
    {
        return $this->sites;
    }
    /**
     * @var \Datum\FrontendBundle\Entity\Author
     */
    private $authors;


    /**
     * Set authors
     *
     * @param \Datum\FrontendBundle\Entity\Author $authors
     * @return SitePaymentGateways
     */
    public function setAuthors(\Datum\FrontendBundle\Entity\Author $authors = null)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return \Datum\FrontendBundle\Entity\Author 
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}

<?php

namespace Datum\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserExtension
 */
class UserExtension
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return UserExtension
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserExtension
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Datum\FrontendBundle\Entity\User
     */
    private $users;


    /**
     * Set users
     *
     * @param \Datum\FrontendBundle\Entity\User $users
     * @return UserExtension
     */
    public function setUsers(\Datum\FrontendBundle\Entity\User $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \Datum\FrontendBundle\Entity\User 
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * @var \Datum\FrontendBundle\Entity\Author
     */
    private $authors;


    /**
     * Set authors
     *
     * @param \Datum\FrontendBundle\Entity\Author $authors
     * @return UserExtension
     */
    public function setAuthors(\Datum\FrontendBundle\Entity\Author $authors = null)
    {
        $this->authors = $authors;

        return $this;
    }

    /**
     * Get authors
     *
     * @return \Datum\FrontendBundle\Entity\Author 
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}

<?php

namespace Datum\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserExtensionType extends AbstractType
{
    public function __construct($author)
    {
        $this->author = $author;
    }

    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('name', 'text', ['label' => 'User', 'attr' => ['class' => 'form-control'] ])
            ->add('email', 'text', ['label' => 'User', 'attr' => ['class' => 'form-control'] ])
            ->add('authors', 'entity', [
                'class'=> 'DatumFrontendBundle:Author',
                'data' => $this->author,
                'label' => 'User', 
                'choice_label' => 'name',
                'attr' => ['class' => 'form-control'] 
                ])
            ;       
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datum\FrontendBundle\Entity\UserExtension',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'userextension';
    }
}

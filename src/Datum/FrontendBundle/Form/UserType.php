<?php

namespace Datum\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder            
            ->add('email', 'text', ['label' => 'User', 'attr' => ['class' => 'form-control'] ])
            ->add('userextensions', 'collection', ['type' => new UserExtensionType($options['author']), 'allow_add' => true, 'by_reference' =>false])
            ;       
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datum\FrontendBundle\Entity\User',
            'author' => ''
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }
}

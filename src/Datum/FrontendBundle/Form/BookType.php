<?php

namespace Datum\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('name', null, ['label' => 'Book name', 'attr' => ['class' => 'form-control'] ])
            ->add('authors', 'entity', [
                    'empty_value' => 'Choose author',
                    'label' => 'Author',
                    'class' => 'Datum\FrontendBundle\Entity\Author',
                    'property' =>  'name',
                    'expanded' => false,
                    'multiple' => false,
                    'attr' => ['class' => 'form-control']
                ])
            ->add('categories', 'entity', [
                    'label' => 'Category',
                    'class' => 'Datum\FrontendBundle\Entity\Category',
                    'property' =>  'name',
                    'expanded' => false,
                    'multiple' => true,
                    'attr' => ['class' => 'form-control']
                ]);       
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datum\FrontendBundle\Entity\Book',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'book';
    }
}

<?php

namespace Datum\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SitePaymentGatewayType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('is_enabled', 'choice', [
                    'empty_value' => 'Choose author',
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => ['class' => 'form-control']
                ])
            ->add('is_testmode', 'choice', [
                    'empty_value' => 'Choose author',
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => ['class' => 'form-control']
                ]);       
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datum\FrontendBundle\Entity\SitePaymentGateways',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'site_payent_gateway';
    }
}

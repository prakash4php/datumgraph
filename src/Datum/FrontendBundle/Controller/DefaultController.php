<?php

namespace Datum\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Datum\FrontendBundle\Form\SitePaymentGatewayType;
use Datum\FrontendBundle\Entity\Menu;
use Datum\FrontendBundle\Entity\User;
use Datum\FrontendBundle\Form\UserType;

class DefaultController extends Controller
{
    /**
     * @Template("DatumFrontendBundle:Default:index.html.twig")
     * @return array
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager(); 
        $oPaymentGateways = $em->getRepository('DatumFrontendBundle:PaymentGateways')->findAll();
        // var_dump($oPaymentGateways);die;
        
        $form = $this->createForm(new SitePaymentGatewayType());
        return [
                 'oPayment' => $oPaymentGateways,
                 'form' => $form->createView()
               ];
    }

    public function menuAction()
    {
        $em = $this->getDoctrine()->getManager();

        $food = new Menu();
        $food->setTitle('Food');

        $fruits = new Menu();
        $fruits->setTitle('Fruits');
        $fruits->setParent($food);

        $vegetables = new Menu();
        $vegetables->setTitle('Vegetables');
        $vegetables->setParent($food);

        $carrots = new Menu();
        $carrots->setTitle('Carrots');
        $carrots->setParent($vegetables);

        $em->persist($food);
        $em->persist($fruits);
        $em->persist($vegetables);
        $em->persist($carrots);
        $em->flush();
    }

    /**
     * @Template("DatumFrontendBundle:Default:embedexample.html.twig")
     * @return array
     */
    public function embedExampleAction(Request $request)
    {
        $user = new User();

        $em = $this->getDoctrine()->getManager();
        $oAuthor = $em->getRepository('DatumFrontendBundle:Author')->find(1);
        $form = $this->createForm(new UserType(), $user, ['author'=>$oAuthor]);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em->persist($user);
            $em->flush();
        }

        return [
                 'form' => $form->createView()
               ];
    }
}

<?php

namespace Datum\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Doctrine\ORM\EntityManager;
use Datum\FrontendBundle\Entity\Book;
use Datum\FrontendBundle\Form\BookType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormView;
class BookController extends Controller
{
    /**
     * @var formFactory
     */
    private $formFactory;

    /**
     * @var entityManager
     */
    private $entityManager;

    /**
     * @var router
     */
    private $router;

    /**
     * @var knpPaginator
     */
    private $knpPaginator;

    /**
     * @var knpPaginator
     */
    private $session;

    /**
     * @var knpPaginator
     */
    private $perPageRecord;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManager $entityManager
     * @param Router $router
     * @param Session $session
     * @param Paginator $knpPaginator
     * @param integer $perPageRecord
     */
    public function __construct(
        FormFactoryInterface $formFactory, 
        EntityManager $entityManager,
        Router $router,
        Session $session,
        $knpPaginator,
        $perPageRecord,
        $errorHandler)
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->session = $session;
        $this->knpPaginator = $knpPaginator;
        $this->perPageRecord = $perPageRecord;
        $this->errorHandler = $errorHandler;
    }

    /**
     * @Template("DatumFrontendBundle:Book:index.html.twig")
     * @return array
     */
    public function indexAction(Request $request)
    {
        $oBook = $this->entityManager->getRepository('DatumFrontendBundle:Book')->findAll();
        $oBookDetail = $this->knpPaginator->paginate($oBook, $request->query->get('page', 1), $this->perPageRecord);
        return [
                 'oBookDetail' => $oBookDetail
               ];
    }

    /**
     * @Template("DatumFrontendBundle:Book:add.html.twig")
     * @return array
     */
    public function addAction(Request $request)
    {
        $oBook = new Book();
        
        $oBookForm = $this->formFactory->create('book', $oBook);

        if ('POST' === $request->getMethod()) {
            $formSuccess = $this->processForm($request, $oBookForm, $oBook);
            //var_dump($formSuccess);die;
            // if ($formSuccess) {
            //     $this->session->getFlashBag()->add('success', 'Record added succesfully');
            //     return new RedirectResponse($this->router->generate('datum_frontend_booklisting'));
            // }
            // else
            // {
                return $formSuccess;
            // }
        }
        return [
                 'oBookForm' => $oBookForm->createView()
               ];
    }

    /**
     * @Template("DatumFrontendBundle:Book:edit.html.twig")
     * @ParamConverter("book", class="DatumFrontendBundle:Book")
     * @return array
     */
    public function editAction(Request $request, $id)
    {
        $oBook = new Book();
        if (isset($id) && $id != '') {         
            $oBook = $this->entityManager->getRepository('DatumFrontendBundle:Book')->find($id);
            $this->checkObjectExist($oBook);
        }

        $oBookForm = $this->formFactory->create('book', $oBook);

        if ('POST' === $request->getMethod()) {
            $formSuccess = $this->processForm($request, $oBookForm, $oBook);

            return $formSuccess;
            // if ($formSuccess) {
            //     $this->session->getFlashBag()->add('success', 'Record updated succesfully');
            //     return new RedirectResponse($this->router->generate('datum_frontend_booklisting'));
            // }
        }

        return [
                 'oBookForm' => $oBookForm->createView()
               ];
    }


    /**     
     * @ParamConverter("book", class="DatumFrontendBundle:Book")
     * @return Response
     */
    public function deleteAction(Request $request, $id)
    {
        $oBook = $this->entityManager->getRepository('DatumFrontendBundle:Book')->find($id);
        
        $this->checkObjectExist($oBook);

        $this->entityManager->remove($oBook);
        $this->entityManager->flush();

        $this->session->getFlashBag()->add('success', 'Record deleted succesfully');
        return new RedirectResponse($this->router->generate('datum_frontend_booklisting'));
    }

    /**
     * @Template("DatumFrontendBundle:Book:show.html.twig")
     * @ParamConverter("book", class="DatumFrontendBundle:Book")
     * @return array
     */
    public function showAction(Request $request, $id)
    {
        $oBookDetail = $this->entityManager->getRepository('DatumFrontendBundle:Book')->find($id);

        $this->checkObjectExist($oBookDetail);

        return [
                 'oBookDetail' => $oBookDetail
               ];
    }

    /**
     * function for process form.
     * @param Request $request
     * @param Form $oBookForm
     * @param integer $bookId
     * @param Book $oBook
     */
    private function processForm($request, $oBookForm, $oBook)
    {

        $oBookForm->handleRequest($request);

        if ($oBookForm->isValid()) {

            $this->entityManager->persist($oBook);
            $this->entityManager->flush();
            
            return true;
        }
        else
        {
           return $this->errorHandler->getFormErrors($oBookForm);
        }
    }

    /**
     * function for check object exist or not.
     * @param Book $oBook
     */
    private function checkObjectExist($oBook)
    {
        if (!$oBook) {
            throw $this->createNotFoundException('Object not exist');
        }
    }

    /**
     * Populate the errors array with any errors in the given FormView
     *
     * @param array $errors  Add errors to this array
     * @param FormView $field Form view that may contain errors
     * @param string $index Optional index for the error in the array
     */
    private function addFormViewErrors(array & $errors, FormView $field, $index = null)
    {
        if (!$index) {
            $index = $field->vars['full_name'];
        }

        foreach ($field->vars['errors'] as $error) {
            $errors[] = [ $index => $error->getMessage() ];
        }

        foreach ($field as $child) {
            $this->addFormViewErrors($errors, $child);
        }
    }
}

<?php

namespace Datum\FrontendBundle\Services;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

/**
 * Mail manager
 */
class MailManager
{
    /**
     * @var mailer
     */
    private $mailer;

    /**
     * @var templating
     */
    private $templating;

    /**
     * @var ssAdminMail
     */
    private $ssAdminMail;

    /**
     * @param Swift_Mailer $mailer
     * @param EngineInterface $templating
     * @param string $ssAdminMail
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $ssAdminMail)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = $ssAdminMail;
        $this->ssAdminMail = $ssAdminMail;
    }

    /**
     * Function createMail
     *
     * @param string $to
     * @param string $subject
     *
     * @todo   Function to create mail content.
     * @access public
     * @author Arpita Jadeja <arpita.rana212@gmail.com>        
     */
    public function createMail($to, $subject) {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->from)
            ->setTo($to)
            ->setContentType("text/html");
        return $message;
    }

    /**
     * Function sendMailToAuthor
     *
     * @param string $toMail
     * @param string $subject
     * @param object $oBook
     *
     * @todo   Function to send mail to student.
     * @access public
     * @author Arpita Jadeja <arpita.rana212@gmail.com>        
     */
    public function sendMailToAuthor($toMail, $subject, $oBook) {    
        $message = $this->createMail($toMail, $subject);        
        $message->setBody(
                $this->templating->render(
                    'DatumFrontendBundle:Email:informAuthor.html.twig',
                    ['oBook' => $oBook]
                )
            );
        $this->mailer->send($message);
    }
}
<?php
namespace Datum\FrontendBundle\Services;
use Symfony\Component\Form\FormView;
class ErrorHandler
{
	public function getFormErrors($form)
	{
		$jsonData['result'] = 'fail';

        $errors = [];
        
        $this->addFormViewErrors($errors, $form->createView());
        $jsonData['errors'] = $errors;

        return new JsonResponse($jsonData);
	}

	/**
     * Populate the errors array with any errors in the given FormView
     *
     * @param array $errors  Add errors to this array
     * @param FormView $field Form view that may contain errors
     */
    private function addFormViewErrors(array & $errors, FormView $field)
    {
        foreach ($field->vars['errors'] as $error) {
            $errors[] = [ $index => $error->getMessage() ];
        }

        foreach ($field as $child) {
            $this->addFormViewErrors($errors, $child);
        }
    }
}